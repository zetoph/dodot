FROM python:3-alpine

RUN addgroup -S serve && adduser -S serve -G serve
USER serve
COPY --chown=serve serve.py /home/serve/

ENTRYPOINT /home/serve/serve.py
