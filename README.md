# DoDoT

## DNS over DNS-over-TLS

Resolves DNS queries over DoT.

### Usage

The two environment variables `BIND_ADDR` and `DOT_SRV` configure the listen address of the service
and the server address of a DoT-enabled DNS server.

```bash
$ BIND_ADDR='localhost:1053' DOT_SRV='1.1.1.1:853' ./serve.py
```

Or use Docker:

```bash
$ docker build -t dodot:0.0.1 .
$ docker run \
    -p 1053:1053 \
	-e BIND_ADDR=0.0.0.0:1053 \
	-e DOT_SRV=1.1.1.1:853 \
	dodot:0.0.1
```

To test the running service, you can use dig:

```bash
dig -p 1053 +tcp example.com @127.0.0.1 
```

### Implementation Details

In its current state the script uses three threads that respond to and manage two thread-safe queues
in order to resolve incoming queries. It uses the terms left-hand side (lhs) and right-hand side
(rhs) to describe the communication participants as well as the queues. On the lhs, a thread
(`INCOMING`) listens for TCP connections from clients (DNS-queries on unencrypted transport) and
places them in the lhs queue (`lhsq`). From there, they are fetched by a second thread
(`TRANSFER`) that writes them to a client socket connected to the rhs, both the lhs connection
(Python TCP socket) and the rhs connection (Python SSLSocket) are placed in the second queue
(`rhsq`). A third thread (`RESPONSE`) retrieves both sockets and sends the rhs data to lhs. This
way, no two threads will ever write on the same socket.

The middle thread `TRANSFER` will try to reuse an existing TLS/SSL connection and only create a new
one if the old one has been closed, resulting in a considerable speed-up:

```
2020-10-06 17:59:29,874 INCOMING [INFO]: Listening on 0.0.0.0:1053
2020-10-06 17:59:29,935 TRANSFER [INFO]: Connected to to DoT server at 1.1.1.1:853
2020-10-06 17:59:33,659 INCOMING [INFO]: Incoming 127.0.0.1:58795
2020-10-06 17:59:33,659 TRANSFER [INFO]: Query -> 1.1.1.1:853
2020-10-06 17:59:33,687 RESPONSE [INFO]: Reply -> 127.0.0.1:58795 in 0.0277s
2020-10-06 17:59:56,511 INCOMING [INFO]: Incoming 127.0.0.1:48153
2020-10-06 17:59:56,511 TRANSFER [INFO]: Reconnecting...
2020-10-06 17:59:56,560 TRANSFER [INFO]: Query -> 1.1.1.1:853
2020-10-06 17:59:56,604 RESPONSE [INFO]: Reply -> 127.0.0.1:48153 in 0.0923s
```

### Improvements

**Security**

The **TLS context** is set on Python's default. Since most of the public DNS servers - and especially
those that implement DNS-over-TLS - are probably supporting the lates cipher suites and TLS
versions, the context configuration could very likely be hardened substantially to avoid down-grade
attacks and known vulnerabilities. Another effective security measure in a case where the rhs is a 
trusted public server would be **certificate pinning** using a SPKI fingerprint, preventing
any on-path attacks.

**Code Quality**

There are currently no tests for the functions in the script and exception/error handling is fairly
minimal as well. This is especially wrong considering the thread-based setup: the the main
thread will just continue running even when two of the three threads have already crashed.

**Caching**

The service could serve DNS queries from cache.
