#!/usr/bin/env python3

"""
A small DNS-over-TLS adapter written in Python. Listens for queries on a TCP socket, passes them on
over TLS to a DoT-enabled DNS server and returns them to the client.

Usage:

    $ ./serve.py

The following environment variables may be configured:

    BIND_ADDR: <interface>:<port>
        The address and port to listen on for new queries. Defaults to 0.0.0.0:1053
    DOT_SRV: <hostname>:<port>
        The address of the DoT enabled DNS server to forward queries to

~-~-~
Author: Christoph Schulthess <christoph.schulthess@posteo.net>
License: GPL-3.0
Date: 10/06/2020
Version: 0.0.1
~-~-~
"""

import threading
import os
import select
import socket
import ssl
import logging
import time
from queue import Queue
from typing import Tuple

DEFAULT_LHS = ("0.0.0.0", 1053)
DEFAULT_RHS = ("1.1.1.1", 853)


def create_tls_socket(
    addr: Tuple[str, int], context: ssl.SSLContext = ssl.create_default_context()
):
    """Returns a non-blocking TLS/SSLSocket."""
    sock = socket.create_connection(addr)
    tls_sock = context.wrap_socket(sock, server_hostname=addr[0])
    tls_sock.setblocking(0)
    return tls_sock


def lhsq_populate(lhsq: Queue, bind_addr: Tuple[str, int]):
    """
    Populates a queue with socket.Socket objects representing incoming TCP connections. Also adds a
    timestamp for performance feedback.

    Parameters
    ----------
        lhsq:
            The queue to populate with incoming connections
        bind_addr:
            The interface and port number to listen to for incoming
            connections
    """
    sock = socket.socket()
    sock.setblocking(0)
    sock.bind(bind_addr)
    sock.listen(1)
    log.info("Listening on %s:%s", bind_addr[0], bind_addr[1])
    while True:
        read_ready, _, _ = select.select([sock], [], [sock])
        if read_ready == [sock]:
            conn, addr = sock.accept()
            log.info("Incoming %s:%s", addr[0], addr[1])
            lhsq.put((conn, time.time()))


def rhsq_populate(lhsq: Queue, rhsq: Queue, rhs_addr: Tuple[str, int]):
    """
    Retrieves connections from read-left-hand-side queue and writes them to right-hand-side TLS/SSL
    socket (wrhs). Both connection objects are then put to read-right-hand-side queue in order to be
    written back to the client connection. The rhs TLS socket is reused as long as it's open and
    re-created otherwise.

    Parameters
    ----------
        lhsq:
            read-left-hand-side queue
        rhsq:
            read-right-hand-side queue
        rhs_addr:
            right-hand-side address to send query to
    """
    wrhs = create_tls_socket(rhs_addr)
    log.info("Connected to to DoT server at %s:%s", rhs_addr[0], rhs_addr[1])
    while True:
        rlhs, t_received = lhsq.get()
        data = rlhs.recv(1024)
        log.debug("Socket %s read: %s bytes", rlhs.fileno(), len(data))
        read_ready, write_ready, _ = select.select([wrhs], [wrhs], [wrhs])
        if read_ready == [wrhs]:
            log.info("Reconnecting...")
            wrhs = create_tls_socket(rhs_addr)
            wrhs.sendall(data)
        elif write_ready == [wrhs]:
            wrhs.sendall(data)
        log.debug("Socket %s wrote: %s bytes", wrhs.fileno(), len(data))
        rhsq.put(((rlhs, wrhs), t_received))
        log.info("Query -> %s", ":".join([str(el) for el in wrhs.getpeername()]))


def write_lhs(rhsq: Queue) -> None:
    """
    Retrieves connections from right-hand-side-queue and writes them back to
    client.
    """
    while True:
        socks, t_received = rhsq.get()
        wlhs, rrhs = socks
        read_ready, _, _ = select.select([rrhs], [], [])
        if read_ready != []:
            data = rrhs.recv(1024)
            log.debug("Socket %s read %s bytes", rrhs.fileno(), len(data))
            wlhs.sendall(data)
            dur = round(time.time() - t_received, 4)
            log.debug("Socket %s wrote: %s bytes", wlhs.fileno(), len(data))
            log.info(
                "Reply -> %s in %ss",
                ":".join([str(el) for el in wlhs.getpeername()]),
                dur,
            )


def init_logging() -> None:
    """Initializes logging."""
    logger = logging.getLogger()
    logger.setLevel(logging.getLevelName("INFO"))
    log_formatter = logging.Formatter(
        "%(asctime)s %(threadName)s [%(levelname)s]: %(message)s"
    )
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)
    return logger


def serve() -> None:
    """
    Entrypoint function
        1. Configure left-hand-side bind address and right-hand-side host
        2. Initialize shared queues
        3. Create threads with target functions (s. above)
        4. Start threads
    """
    try:
        lhs = os.environ["BIND_ADDR"].split(":")
        lhs = (lhs[0], int(lhs[1]))
    except KeyError:
        lhs = DEFAULT_LHS
    try:
        rhs = os.environ["DOT_SRV"].split(":")
        rhs = (rhs[0], int(rhs[1]))
    except KeyError:
        rhs = DEFAULT_RHS

    lhs_queue = Queue()
    rhs_queue = Queue()

    lhs_reader = threading.Thread(
        target=lhsq_populate,
        name="INCOMING",
        args=(
            lhs_queue,
            lhs,
        ),
    )
    rhs_writer = threading.Thread(
        target=rhsq_populate,
        name="TRANSFER",
        args=(
            lhs_queue,
            rhs_queue,
            rhs,
        ),
    )
    lhs_writer = threading.Thread(target=write_lhs, name="RESPONSE", args=(rhs_queue,))

    lhs_reader.start()
    rhs_writer.start()
    lhs_writer.start()


if __name__ == "__main__":
    log = init_logging()
    serve()
